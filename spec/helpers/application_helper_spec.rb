require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "Page-Title" do
    context "ルートディレクトリ" do
      it "ベースタイトルが表示がされる" do
        expect(helper.full_title('')).to eq('BIGBAG Store')
      end
    end
    context "ルートディレクトリ以外" do
      it "ベースタイトル - ページタイトルが表示される" do
        expect(helper.full_title('Page-Title')).to eq('BIGBAG Store - Page-Title')
      end
    end
  end
end
