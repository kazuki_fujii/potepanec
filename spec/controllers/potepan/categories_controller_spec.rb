require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxon) { create(:taxon) }
  let(:products) { 
    create_list(:product,10) do |product|
      product.taxons << taxon
    end
  }
  describe "GET #show" do
    before do
      get :show, params: { id: taxon.id }
    end

    it 'showテンプレートが表示される' do
      expect(response).to render_template :show
    end

    it 'インスタンス変数 @taxon が期待した値を持つ' do
      expect(assigns(:taxon)).to eq taxon
    end

    it 'インスタンス変数 @products が期待した値を持つ' do
      expect(assigns(:products)).to eq products
    end

    it "リクエストが200となる" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end
end
