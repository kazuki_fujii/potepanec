require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let(:product) { create(:product) }
  describe "GET #show" do
    before do
      get :show, params: { id: product.id }
    end

    it 'showテンプレートが表示される' do
      expect(response).to render_template :show
    end

    it 'インスタンス変数が期待した値を持つ' do
      expect(assigns(:product)).to eq product
    end

    it "リクエストが200となる" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "TEST related_products" do
    let(:taxonomy_categories) {create(:taxonomy, name: 'Categories')}
    let(:taxonomy_brands) {create(:taxonomy, name: 'Brands')}

    let(:bags) { create(:taxon, name: 'Bags', taxonomy: taxonomy_categories) }
    let(:mugs) { create(:taxon, name: 'Mugs', taxonomy: taxonomy_categories) }
    let(:ruby) { create(:taxon, name: 'Ruby', taxonomy: taxonomy_brands) }
    let(:rails) { create(:taxon, name: 'Rails', taxonomy: taxonomy_brands) }

    let(:ruby_bags) { create_list(:product, 2, taxons: [ruby, bags]) }
    let(:ruby_mugs) { create_list(:product, 5, taxons: [ruby, mugs]) }
    let(:rails_bags) { create_list(:product, 6, taxons: [rails, bags]) }
    let(:rails_mugs) { create_list(:product, 4, taxons: [rails, mugs]) }

    it 'インスタンス変数が期待した値を持つ' do
      ruby_bag_product = ruby_bags.first
      related_products = ruby_bags + ruby_mugs + rails_bags - [ruby_bag_product]
      get :show, params: { id: ruby_bag_product.id }
      expect(assigns(:related_products)).to match_array related_products[0..3]
    end

    context "関連商品が3個の場合" do
      it '関連商品を3個表示' do
        rails_mugs_product = rails_mugs.first
        get :show, params: { id: rails_mugs_product.id }
        expect(assigns(:related_products).count).to eq 3
      end
    end

    context "関連商品が4個の場合" do
      it '関連商品を4個表示' do
        ruby_mugs_product = ruby_mugs.first
        get :show, params: { id: ruby_mugs_product.id }
        expect(assigns(:related_products).count).to eq 4
      end
    end

    context "関連商品が5個の場合" do
      it '関連商品を4個まで表示' do
        rails_bags_product = rails_bags.first
        get :show, params: { id: rails_bags_product.id }
        expect(assigns(:related_products).count).to eq 4
      end
    end
  end
end
