class Potepan::ProductsController < ApplicationController
  PRODUCTS_LIMIT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.include_image_price.related_products(@product).limit(PRODUCTS_LIMIT)
  end
end
