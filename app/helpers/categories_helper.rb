module CategoriesHelper
  def products_count_with_category(taxon)
    if taxon.leaf?
      taxon.products.count
    else
      Spree::Product.in_taxon(taxon).count
    end
  end
end
