Spree::Product.class_eval do
  scope :include_image_price, -> { includes(master: [:images, :default_price]) }
  scope :related_products, -> (product) {
    joins(:taxons).where(spree_taxons: { id: product.taxons.ids }).distinct.where.not(id: product.id)
  }
end
